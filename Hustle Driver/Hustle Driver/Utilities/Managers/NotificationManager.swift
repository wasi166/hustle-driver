//
//  NotificationManager.swift
//  Hustle
//
//  Created by  Wasi Tariq on 18/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import AVFoundation

enum RideStatusTypes: String{
    case busy = "Busy"
    case accepted = "Accepted"
    case cancelled = "Cancelled by driver"
    case arrived = "Arrived"
    case inRide = "Started"
    case finished = "Finished"
    case paid = "Paid"
    case rideRequest = "Requested"
    case customerCancelled = "Cancelled by customer after accepting"
}

class NotificationManager{
    static let shared = NotificationManager()
    
    private init(){}
    
    func didRecieveNotification(data: Dictionary<String, Any>){
        if let notification = readAndParseNotificationData(dict: data),
            let status = notification.ride?.rideStatus?.Title{
            postInternalNotification(rideStatus: status, object: notification)
        }
    }
    
    private func readAndParseNotificationData(dict: Dictionary<String, Any>) -> NotificationDataModel?{
        do{
            let data = (dict["data"] as! String).data(using: .utf8)
            let notificationModel = try JSONDecoder().decode(NotificationDataModel.self, from: data!)
            return notificationModel
        }catch{
            debugPrint("error in decoding notification data")
        }
        return nil
    }
    
    private func postInternalNotification(rideStatus: String, object: NotificationDataModel){
        if let ridestatusType = RideStatusTypes(rawValue: rideStatus){
            switch ridestatusType{
            case .rideRequest:
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                NotificationCenter.default.post(name: .newRideRequest, object: object, userInfo: nil)
                
            case .customerCancelled:
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                NotificationCenter.default.post(name: .customerCancelled, object: object, userInfo: nil)
                
            case .paid:
                break
                
            default:
                break
            }
        }
    }
    
    
}
