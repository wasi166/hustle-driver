//
//  UIConstants.swift
//  Hustle
//
//  Created by  Wasi Tariq on 30/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct UIConstants{
    
    struct Storyboards{
        static let registration = "Registrations"
        static let ride = "Ride"
    }
    
    struct Segues{
        struct Registration{
            static let startVCToSignup = "startVCToSignup"
            static let startVCToSignin = "startVCToSignin"
            static let signinVCtoSignupVC = "signinVCToSignupVC"
        }
        
        struct Ride{
            static let presentSideMenu = "presentSideMenuFormMap"
            static let presentSelectionFromMap = "presentSelectionFromMap"
            static let showRateUserFromMap = "showRateUserFromMap"
            static let showRateUserFromInRide = "showRateUserFromInRide"
            static let showInRideFromMap = "mapViewControllerToInRideViewController"
            static let presentWalletFromMap = "presentWalletFromMap"
            static let presentEditProfileFromMap = "presentEditProfileFromMap"
            static let presentRideHistory = "presentRideHistoryFromMap"
            static let showArriveLocationFromMap = "MapVCToArriveToLocationVC"
            static let showInRideFromArrivingLocation = "ArrivingLocationToInRideVC"
        }
    }
    
    struct ViewControllerIdentifiers {
        struct Registrations {
            static let startNavigationController = "startNavigationController"
            static let startVC = "StartViewController"
        }
        
        struct Ride {
            static let mapNavigationVC = "MapNavigationController"
            static let mapVC = "MapViewController"
            static let inRideVC = "InRideViewController"
        }
    }
}
