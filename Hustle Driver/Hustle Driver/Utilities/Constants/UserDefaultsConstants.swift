//
//  UserDefaultsConstants.swift
//  Hustle
//
//  Created by  Wasi Tariq on 12/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

class UserDefaultConstants{
    
    public static let LocationPermissionAsked = "hasAskedForLocationPermission"
    
}
