//
//  ConfigurationFileConstant.swift
//  Hustle
//
//  Created by  Wasi Tariq on 14/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

enum FileType: String{
    case sideMenu = "SideMenu"
    case sideMenuDemo = "side-menu-demo"
}

class ConfigurationsConstants{
    static let fileType = "plist"
}
