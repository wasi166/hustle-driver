//
//  ServiceConstants.swift
//  Hustle
//
//  Created by  Wasi Tariq on 03/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

class ServiceConstants{
    
    private static let devBaseURL = "https://hustledevapp.com/api/v1"
    private static let prodBaseURL = "https://hustleprodapp.com/api/v1"
    
    // base URL
    private static let baseURL = ServiceConstants.prodBaseURL
    
    // validate number
    static let validateNumberEndpoint = ServiceConstants.baseURL+"/ValidateNumber"
    
    // Verify number
    static let verifyNumberEndpoint = ServiceConstants.baseURL+"/VerifyDriverNumber"
    
    // Registration
    static let registerEndpoint = ServiceConstants.baseURL+"/RegisterDriver"
    
    // Estimate fare
    static let estimateFareEndpoint = ServiceConstants.baseURL+"/EstimateFare"
 
    // Request Ride
    static let requestRideEndpoint = ServiceConstants.baseURL+"/RequestForRide"
    
    // Get Ride Details
    static let rideDetailsEndPoint = ServiceConstants.baseURL+"/GetRideDetails"
    
    // Cancel Current Ride
    static let cancelCurrentRideEndpoint = ServiceConstants.baseURL+"/CancelRideByCustomer"
    
    // rate driver
    static let rateDriverEndpoint = ServiceConstants.baseURL+"/SaveCustomerFeedback"
    
    // get wallet amount
    static let walletAmountEndoint = ServiceConstants.baseURL+"/GetCustomerWalletamount"
    
    // save user profile
    static let saveProfileEndpoint = ServiceConstants.baseURL+"/UpdateCustomerProfile"
    
    // user pending rides
    static let pendingRidesEndpoint = ServiceConstants.baseURL+"/CheckDriverPendingRide"
    
    // Driver Ride History
    static let rideHistoryEndpoint = ServiceConstants.baseURL+"/GetDriverRidesHistory"
    
    // update driver location
    static let updateDriverLocation = ServiceConstants.baseURL+"/UpdateDriverLocation"
    
    // change driver status
    static let changeDriverStatusEndpoint = ServiceConstants.baseURL+"/ChangeDriverStatus"
    
    // accept a ride
    static let acceptRideEndPoint = ServiceConstants.baseURL+"/AcceptRide"
    
    // cancel ride by driver
    static let cancelRideByDriver = ServiceConstants.baseURL+"/CancelRideByDriver"
    
    // mark driver arrived
    static let driverArrivedEndpoint = ServiceConstants.baseURL+"/MarkArrived"
    
    // start ride
    static let startRideEndpoint = ServiceConstants.baseURL+"/StartRide"
    
    // end ride
    static let endRideEndpoint = ServiceConstants.baseURL+"/FinishRide"
    
    // save ride fare
    static let saveRideFare = ServiceConstants.baseURL+"/SaveFareForRide"
    
    // driver feedback
    static let driverFeebackEndpoint = ServiceConstants.baseURL+"/SaveDriverFeedback"
}
