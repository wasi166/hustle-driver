//
//  Notification+Hustle.swift
//  Hustle
//
//  Created by  Wasi Tariq on 12/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    // triggers when app receives location for the first time in app cycle.
    static let receivedLocationFirstTime = Notification.Name("com.hustle.receivedLocationFirstTime")
    
    // triggers when app receives location every time
    static let receivedUpdatedLocation = Notification.Name("com.hustle.receivedUpdatedLocation")
 
    // triggers when user's ride has been accepted by any driver
    static let driverAcceptedRide = Notification.Name("com.hustle.driverAcceptedRide")
    
    // triggers when driver has arrived at the requested location from the user
    static let driverArrived = Notification.Name("com.hustle.driverArrived")
    
    // triggers when driver has started the ride
    static let rideStarted = Notification.Name("com.hustle.rideStarted")
    
    // triggers when driver has cancelled the ride after accepting it.
    static let driverCancelledRide = Notification.Name("com.hustle.driverCancelledRide")
    
    // triggers when drivers ended the ride.
    static let rideEnded = Notification.Name("com.hustle.rideEnded")
    
    // triggers when driver receives a new ride request
    static let newRideRequest = Notification.Name("com.hustle.newRideRequest")
    
    // triggers when customer has cancelled the ride
    static let customerCancelled = Notification.Name("com.hustle.customerCancelled")
}
