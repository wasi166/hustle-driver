//
//  UITextField+Theme.swift
//  Hustle
//
//  Created by  Wasi Tariq on 30/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

extension UITextField{
    func makeThemedTextField(){
        self.layer.cornerRadius = 10.0
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        self.layer.shadowOpacity = 0.3
    }
    
    func configureLeftView(view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 20))){
        view.layer.cornerRadius = self.layer.cornerRadius
        self.leftView = view
        self.leftViewMode = .always
    
    }
}
