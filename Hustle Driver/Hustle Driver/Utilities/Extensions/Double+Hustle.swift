//
//  Double+Hustle.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 10/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

extension Double{
    func round(toPlaces: Int) -> Double {
        let p = log10(abs(self))
        let f = pow(10, p.rounded() - Double(toPlaces) + 1)
        let rnum = (self / f).rounded() * f
        return rnum
    }
}
