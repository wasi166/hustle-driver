//
//  SaveFareView.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 14/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol SaveFareDelegate: class {
    func payFare(amount: Double)
}

class SaveFareView: BaseView {

    static let nibName = "SaveFare"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var SARLabel: UILabel!
    @IBOutlet weak var oweLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    
    weak var delegate: SaveFareDelegate?
    var totalFare: Double = 0.0{
        didSet{
            oweLabel.text = "He owe you \(self.totalFare) SAR"
        }
    }
    
    override func configureUI() {
        super.configureUI()
        // title label
        titleLabel.textColor = AppTheme.colorText
        titleLabel.font = AppTheme.fontHeading(ofSize: 16.0)
        // text field
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.cornerRadius = 10.0
        textField.font = AppTheme.fontHeading(ofSize: 14.0)
        textField.textColor = AppTheme.colorText
        // SAR Label
        SARLabel.textColor = AppTheme.colorText
        SARLabel.font = AppTheme.fontHeading(ofSize: 16.0)
        // owe label
        oweLabel.textColor = AppTheme.colorText
        oweLabel.font = AppTheme.fontText(ofSize: 12.0)
        oweLabel.text = "He owe you \(totalFare) SAR"
        // error label
        errorLabel.textColor = .red
        errorLabel.font = AppTheme.fontText(ofSize: 12.0)
        errorLabel.isHidden = true
        // pay button
        payButton.setTitleColor(.white, for: .normal)
        payButton.backgroundColor = AppTheme.colorGreen
        payButton.layer.cornerRadius = 10.0
        payButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
    }

    
    @IBAction func didTapPayButton(_ sender: Any) {
        if let amount = Double(textField.text ?? ""){
            if amount < totalFare{
                errorLabel.isHidden = false
            }else{
                errorLabel.isHidden = true
                delegate?.payFare(amount: amount)
            }
        }
    }
}
