//
//  RideRequestView.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 08/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol RideRequestDelegate: class {
    func didAcceptRequest(with model: NotificationDataModel)
    func didRejectRequest(with model: NotificationDataModel)
}

class RideRequestView: BaseView {

    static let nibName = "RideRequest"
    static let width = 320
    static let height = 300
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickupPointTitleLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var pickupLocationLabel: UILabel!
    @IBOutlet weak var dropoffTitleLabel: UILabel!
    @IBOutlet weak var dropoffLabel: UILabel!
    @IBOutlet weak var estimateDistanceLabel: UILabel!
    @IBOutlet weak var estimateFareLabel: UILabel!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet var separatorViews: [UIView]!
    
    weak var delegate: RideRequestDelegate?
    var rideModel: NotificationDataModel?{
        didSet{
            if let model = self.rideModel{
                print(model)
                self.setDataValues(with: model)
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        // Drawing code
    }

    override func configureUI() {
        super.configureUI()
        //title view
        titleView.backgroundColor = AppTheme.colorPrimary
        //title label
        titleLabel.textColor = .white
        titleLabel.font = AppTheme.fontHeading(ofSize: 16.0)
        // location title labels
        [pickupPointTitleLabel, dropoffTitleLabel].forEach { (label) in
            label?.textColor = AppTheme.colorText
            label?.font = AppTheme.fontText(ofSize: 12.0)
        }
        // separator views
        separatorViews.forEach { (view) in
            view.backgroundColor = AppTheme.colorText
        }
        // location label
        [pickupLocationLabel, dropoffLabel].forEach { (label) in
            label?.font = AppTheme.fontText(ofSize: 16.0)
        }
        // distance label
        distanceLabel.textColor = AppTheme.colorSecondary
        distanceLabel.font = AppTheme.fontText(ofSize: 16.0)
        // estimate labels
        [estimateDistanceLabel, estimateFareLabel].forEach { (label) in
            label?.textColor = AppTheme.colorText
            label?.font = AppTheme.fontText(ofSize: 14.0)
        }
        // reject button
        rejectButton.setTitleColor(AppTheme.colorRed, for: .normal)
        rejectButton.titleLabel?.font = AppTheme.fontText(ofSize: 16.0)
        // accept button
        acceptButton.setTitleColor(.white, for: .normal)
        acceptButton.backgroundColor = AppTheme.colorSecondary
        acceptButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        acceptButton.layer.cornerRadius = 10.0
    }
    
    private func setDataValues(with model: NotificationDataModel){
        pickupLocationLabel.text = model.ride?.source?.Name ?? "unknown"
        dropoffLabel.text = model.ride?.destination?.Name ?? "unknown"
    }
    
    
    @IBAction func didTapRejectButton(_ sender: Any) {
        if let model = self.rideModel{
            self.delegate?.didRejectRequest(with: model)
        }
    }
    
    @IBAction func didTapAcceptButton(_ sender: Any) {
        if let model = self.rideModel{
            self.delegate?.didAcceptRequest(with: model)
        }
    }
}
