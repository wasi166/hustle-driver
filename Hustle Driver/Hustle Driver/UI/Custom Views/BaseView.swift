//
//  BaseView.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 08/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class BaseView: UIView {


    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
     
    override func draw(_ rect: CGRect) {
        // Drawing code
    }

    func configureUI(){
        
    }
    
}
