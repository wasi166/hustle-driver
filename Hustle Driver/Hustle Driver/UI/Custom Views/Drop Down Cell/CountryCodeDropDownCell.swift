//
//  CountryCodeDropDownCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 06/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import DropDown

class CountryCodeDropDownCell: DropDownCell{
    static let nibName = "CountryCodeDropDownCell"
    @IBOutlet weak var logoImageView: UIImageView!
}
