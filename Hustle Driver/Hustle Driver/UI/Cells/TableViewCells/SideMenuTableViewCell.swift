//
//  SideMenuTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 14/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: BaseTableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func initializeUI() {
        self.titleLabel.textColor = AppTheme.colorText
        self.titleLabel.font = AppTheme.fontText(ofSize: 14.0)
    }
    
    override func configure(data: Any, indexPath: IndexPath) {
        if let data = data as? Dictionary<String, Any>{
            titleLabel.text = data["title"] as? String
            iconImageView.image = UIImage(named: data["image"] as! String)
        }
    }
}
