//
//  BaseTableViewCell.swift
//  Hustle
//
//  Created by  Wasi Tariq on 14/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

struct BaseCellConstant {
    static let Identifier = "Identifier"
}

class BaseTableViewCell: UITableViewCell {
    
    @objc public var baseDelegate:(Any)? = nil;
    
    @objc func configure(data: Any , indexPath: IndexPath){
        assertionFailure("Subclass must override this method")
    }
    
    @objc class var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.initializeUI()
    }
    
    func initializeUI(){
        
    }
}
