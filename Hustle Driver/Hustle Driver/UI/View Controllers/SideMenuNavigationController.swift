//
//  SideMenuNavigationController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 31/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuNavigationController: UISideMenuNavigationController{
    
    override func awakeFromNib(){
        super.awakeFromNib()
        self.leftSide = true
    }
}
