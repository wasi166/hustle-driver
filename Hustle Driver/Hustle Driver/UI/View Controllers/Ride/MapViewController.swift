//
//  MapViewController.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 25/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapViewController: BaseViewController, RideRequestDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var statusSwitch: UISwitch!
    @IBOutlet weak var sideMenuButton: UIButton!
    
    private var rideRequestView: RideRequestView?
    
    private let location = LocationManager.shared
    private var currentLocationMarker = GMSMarker()
    private let viewModel = MapViewModel()
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        viewModel.baseDelegate = self
        super.viewDidLoad()
        self.addLocationNotificationObserver()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func configureUI() {
        super.configureUI()
        self.configureMapView()
        self.configureRideRequestView()
        [sideMenuButton].forEach { (view) in
            self.mapView.bringSubviewToFront(view)
        }
    }
    
    private func configureMapView(){
        self.mapView.isMyLocationEnabled = true
        // current location marker
        currentLocationMarker.map = self.mapView
        currentLocationMarker.icon = #imageLiteral(resourceName: "driver-marker")
        
        navigateToCurrentLocation()
        addLocationNotificationObserver()
    }
    
    private func configureRideRequestView(){
        rideRequestView = Bundle.main.loadNibNamed(RideRequestView.nibName, owner: self, options: nil)![0] as? RideRequestView
        self.mapView.addSubview(rideRequestView!)
        rideRequestView!.translatesAutoresizingMaskIntoConstraints = false
        let bottomConstraint = NSLayoutConstraint(item: rideRequestView!, attribute: .bottom, relatedBy: .equal, toItem: mapView, attribute: .bottom, multiplier: 1.0, constant: -20.0)
        let centerConstraint = NSLayoutConstraint(item: rideRequestView!, attribute: .centerX, relatedBy: .equal, toItem: self.mapView, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        NSLayoutConstraint.activate([bottomConstraint, centerConstraint])
        self.mapView.addConstraints([bottomConstraint, centerConstraint])
        rideRequestView?.heightAnchor.constraint(equalToConstant: CGFloat(RideRequestView.height)).isActive = true
        rideRequestView?.widthAnchor.constraint(equalToConstant: CGFloat(RideRequestView.width)).isActive = true
        rideRequestView?.alpha = 0.0
        rideRequestView?.layer.cornerRadius = 10.0
        rideRequestView?.clipsToBounds = true
        rideRequestView?.delegate = self
        self.mapView.bringSubviewToFront(rideRequestView!)
    }
    
    private func addLocationNotificationObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCurrentLocation), name: .receivedLocationFirstTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(receivedUpdatedLocation), name: .receivedUpdatedLocation, object: nil)
    }
    
    
    @objc private func navigateToCurrentLocation(){
        if let currentLocation = location.lastReceivedLocation(){
            self.mapView.animate(to: GMSCameraPosition(latitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 15.0))
            // removing observer because it has got the user location.
            // why removing?
            // Well i guess i can save memory by doing that 😇 , maybe 🙄
            NotificationCenter.default.removeObserver(self, name: .receivedLocationFirstTime, object: nil)
        }
    }
    
    @objc private func receivedUpdatedLocation(){
        if let currentLocation = location.lastReceivedLocation(){
            self.currentLocationMarker.position = currentLocation
            self.currentLocationMarker.rotation = location.lastReceivedLocationDirection()
            self.mapView.isMyLocationEnabled = false
        }
    }
    
    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.Segues.Ride.showArriveLocationFromMap{
            if let model = sender as? NotificationDataModel{
                (segue.destination as! ArrivingToLocationViewController).dataModel = model
            }
        } else if segue.identifier == UIConstants.Segues.Ride.showInRideFromMap{
            if let model = sender as? NotificationDataModel{
                (segue.destination as! InRideViewController).dataModel = model
            }
        } else if segue.identifier == UIConstants.Segues.Ride.showRateUserFromMap{
            if let model = sender as? NotificationDataModel{
                (segue.destination as! RateUserViewController).dataModel = model
            }
        }
    }
    
    //MARK:- UI Updates
    private func showRideRequestView(with model: NotificationDataModel){
        rideRequestView?.rideModel = model
        rideRequestView!.transform = CGAffineTransform(translationX: 0, y: 20)
        UIView.animate(withDuration: 0.5) {
            self.rideRequestView?.transform = .identity
            self.rideRequestView?.alpha = 1.0
        }
    }
    
    internal func hideRideRequestView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.rideRequestView?.transform = CGAffineTransform(translationX: 0, y: -20)
            self.rideRequestView?.alpha = 0.0
        }) { (_) in
            self.rideRequestView?.transform = .identity
        }
    }
    
    //MARK:- IBActions
    @IBAction func didChangeOnlineStatusValue(_ sender: Any) {
        self.showLoader()
        viewModel.changeOnlineStatus(success: {
            self.hideLoader()
          self.showMessage(title: "Success", message: "Successfully updated the online status", type: .success)
        }){
            self.statusSwitch.isOn = !self.statusSwitch.isOn
            self.hideLoader()
            self.showMessage(title: "Error", message: "error in changing the status", type: .error)
        }
    }
    @IBAction func didTapSideMenuButton(_ sender: Any) {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentSideMenu, sender: self)
    }
    
    //MARK:- Ride Request Delegate
    func didAcceptRequest(with model: NotificationDataModel) {
        if let rideID = model.ride?.Id{
            self.showLoader()
            viewModel.acceptRide(with: rideID, success: {
                self.hideLoader()
                self.hideRideRequestView()
                SessionManager.shared.setCurrentTrip(id: rideID)
                self.performSegue(withIdentifier: UIConstants.Segues.Ride.showArriveLocationFromMap, sender: model)
            }) {
                self.hideRideRequestView()
                self.hideLoader()
                self.showMessage(title: "Error", message: "Ride not accepted maybe because accepted by another driver", type: .error)
            }
        }
    }
    
    func didRejectRequest(with model: NotificationDataModel) {
        self.showMessage(title: "Success", message: "Rejected the ride request", type: .success)
        self.hideRideRequestView()
    }
    
}

extension MapViewController: MapViewDelegate{
    func presentNewRideRequest(with model: NotificationDataModel) {
        self.showRideRequestView(with: model)
    }
    
    func presentArrivingVC(with model: NotificationDataModel) {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.showArriveLocationFromMap, sender: model)
    }
    
    func presentInRideVC(with model: NotificationDataModel) {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.showInRideFromMap, sender: model)
    }
    
    func presentRateUserVC(with model: NotificationDataModel) {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.showRateUserFromMap, sender: model)
    }
}
