//
//  RateUserViewController.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 14/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import Cosmos
import EasyPopUp

class RateUserViewController: BaseViewController {

    @IBOutlet var iconViews: [UIImageView]!
    @IBOutlet var titleLabels: [UILabel]!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var reportProblemButton: UIButton!
    @IBOutlet weak var sendRateButton: UIButton!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var rateTripLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var fareLabel: UILabel!
    @IBOutlet weak var paidButton: UIButton!
    
    private var popup: EasyPopup?
    private let viewModel = RateUserViewModel()
    var dataModel: NotificationDataModel!
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        viewModel.dataModel = dataModel
        viewModel.baseDelegate = self
        super.viewDidLoad()
        updateValues()
    }
    
    override func configureUI() {
        super.configureUI()
        self.view.backgroundColor = AppTheme.colorPrimary
        // icon views
        iconViews.forEach { (imageView) in
            imageView.tintColor = .white
        }
        // title labels
        titleLabels.forEach { (label) in
            label.textColor = .white
            label.font = AppTheme.fontText(ofSize: 12.0)
        }
        // fare label
        fareLabel.textColor = .white
        fareLabel.font = AppTheme.fontHeading(ofSize: 40.0)
        // paid button
        paidButton.backgroundColor = AppTheme.colorSecondary
        paidButton.setTitleColor(.white, for: .normal)
        paidButton.layer.cornerRadius = 10.0
        paidButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        // distance & duration label
        [distanceLabel, durationLabel].forEach { (label) in
            label?.textColor = .white
            label?.font = AppTheme.fontHeading(ofSize: 16.0)
        }
        
        // rate trip label
        rateTripLabel.textColor = AppTheme.colorText
        rateTripLabel.font = AppTheme.fontText(ofSize: 12.0)
        // skip button
        skipButton.setTitleColor(.white, for: .normal)
        skipButton.titleLabel?.font = AppTheme.fontText(ofSize: 16)
        // send rate button
        sendRateButton.backgroundColor = AppTheme.colorSecondary
        sendRateButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16)
        sendRateButton.setTitleColor(.white, for: .normal)
        sendRateButton.layer.cornerRadius = 10
        // report problem button
        reportProblemButton.setTitleColor(.white, for: .normal)
        reportProblemButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16)
        reportProblemButton.backgroundColor = AppTheme.colorRed
        reportProblemButton.layer.cornerRadius = 10.0
    }
    
    private func updateValues(){
        fareLabel.text = viewModel.totalFare()
        distanceLabel.text = viewModel.totalDistanceOfRide()
        durationLabel.text = viewModel.totalDurationOfRide()
    }
    
    //MARK:- IBActions
    @IBAction func didTapPaidButton(_ sender: Any) {
        viewModel.didTapPaidButton()
    }
    
    @IBAction func didTapSkipButton(_ sender: Any) {
        if viewModel.canDismiss(){
            self.navigationController?.popToRootViewController(animated: true)
        }else{
            showMessage(title: "Error", message: "Payment must be completed before skipping", type: .error)
        }
    }
    
    @IBAction func didTapSendRateButton(_ sender: Any) {
        if rateView.rating < 1{
            showMessage(title: "Error", message: "Rating should be between 1 to 5", type: .error)
        }else{
            showLoader()
            viewModel.sendRate(rating: Int(rateView.rating), success: {
                self.hideLoader()
                self.navigationController?.popToRootViewController(animated: true)
            }) {
                self.hideLoader()
                self.showMessage(title: "Error", message: "Error in rating user", type: .error)
            }
        }
    }
    
    @IBAction func didTapReportProblemButton(_ sender: Any) {
    }
}

extension RateUserViewController: RateUserDelegate, SaveFareDelegate{
    
    func presentSaveFareView(fare: Double) {
        let fareView = Bundle.main.loadNibNamed(SaveFareView.nibName, owner: self, options: nil)![0] as! SaveFareView
        fareView.delegate = self
        fareView.totalFare = fare
        popup = EasyPopup(superView: self.view, viewTopop: fareView)
        popup!.showPopup()
    }
    
    func didSaveFareSuccessfully() {
        popup?.removePopup()
        paidButton.backgroundColor = AppTheme.colorGreen
    }
    
    func failToSaveFare() {
        showMessage(title: "Error", message: "Error in saving fare", type: .error)
    }
    
    func payFare(amount: Double) {
        viewModel.saveRideFare(amount: amount)
    }
}
