//
//  InRideViewController.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 11/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps

class InRideViewController: BaseViewController {

    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet var titleLabels: [UILabel]!
    @IBOutlet var iconImageViews: [UIImageView]!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dropTitleLabel: UILabel!
    @IBOutlet weak var dropLocationLabel: UILabel!
    @IBOutlet weak var endTripButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    private var wayPoints = [CLLocationCoordinate2D]()
    var dataModel: NotificationDataModel!
    private let viewModel = InRideViewModel()
    override func viewDidLoad() {
        viewModel.baseDelegate = self
        self.baseModel = viewModel
        viewModel.dataModel = dataModel
        super.viewDidLoad()
        self.wayPoints = SessionManager.shared.getWayPoints()
    }

    override func configureUI() {
        super.configureUI()
        // navigation View
        navigationView.backgroundColor = AppTheme.colorPrimary
        // info view
        infoView.backgroundColor = AppTheme.colorPrimary
        // title labels
        titleLabels.forEach { (label) in
            label.textColor = AppTheme.colorText
            label.font = AppTheme.fontText(ofSize: 12.0)
        }
        // icon image views
        iconImageViews.forEach { (image) in
            image.tintColor = .white
        }
        // drop title label
        dropTitleLabel.textColor = .white
        dropTitleLabel.font = AppTheme.fontText(ofSize: 12.0)
        // drop location label
        dropLocationLabel.textColor = .white
        dropLocationLabel.font = AppTheme.fontText(ofSize: 14.0)
        // end trip button
        endTripButton.setTitleColor(.white, for: .normal)
        endTripButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        endTripButton.layer.cornerRadius = 10.0
        endTripButton.backgroundColor = AppTheme.colorSecondary
        // title label
        titleLabel.textColor = .white
        titleLabel.font = AppTheme.fontHeading(ofSize: 24.0)
    }

    private func drawCurrentLocationMarker(){
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "driver-marker")
        marker.map = self.mapView
        marker.position = LocationManager.shared.lastReceivedLocation()!
    }
    
    private func drawDestinationMarker(){
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "drop-marker")
        marker.map = self.mapView
        marker.position = viewModel.destinationLocation()
    }
    
    @IBAction func didTapNavigationItem(_ sender: Any) {
        if let url = viewModel.navigationLinkForRide(){
            if UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func didTapEndTripButton(_ sender: Any) {
        viewModel.didTapEndRideButton()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? RateUserViewController{
            destination.dataModel = viewModel.dataModel
        }
    }
}

//MARK:- InRide Delegate
extension InRideViewController: InRideDelegate{
    func updateValues() {
        dropLocationLabel.text = viewModel.dropOffLocationName()
        distanceLabel.text = viewModel.remainingDistance()
        timeLabel.text = viewModel.remainingTime()
    }
    
    func didGetUpdatedLocation() {
        mapView.drawPath(fromLocation: LocationManager.shared.lastReceivedLocation()!, toLocation: viewModel.destinationLocation()) {
            self.drawCurrentLocationMarker()
            self.drawDestinationMarker()
        }
    }
    
    func endTrip() {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.showRateUserFromInRide, sender: nil)
    }

}
