//
//  ArrivingToLocationViewController.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 07/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ArrivingToLocationViewController: BaseViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var pickupLocationTitleLabel: UILabel!
    @IBOutlet weak var pickupLocationLabel: UILabel!
    @IBOutlet weak var dropoffTitleLabel: UILabel!
    @IBOutlet weak var dropoffLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cashLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var infoView: UIView!
    
    private let statusLabel = UILabel()
    
    var dataModel: NotificationDataModel!
    private let viewModel = ArrivingToLocationViewModel()
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        viewModel.baseDelegate = self
        viewModel.dataModel = self.dataModel
        super.viewDidLoad()
    }

    override func configureUI() {
        super.configureUI()
        configureStatusView()
        // info view
        infoView.backgroundColor = AppTheme.colorPrimary
        // profile imageview
        userProfileImage.layer.cornerRadius = userProfileImage.bounds.height/2
        userProfileImage.clipsToBounds = true
        // name label
        nameLabel.textColor = .white
        nameLabel.font = AppTheme.fontText(ofSize: 16.0)
        nameLabel.text = viewModel.nameOfUser()
        // rating labek
        ratingLabel.textColor = .white
        ratingLabel.font = AppTheme.fontText(ofSize: 12.0)
        ratingLabel.text = viewModel.ratingOfUser()
        // title labels
        [pickupLocationTitleLabel, dropoffTitleLabel].forEach { (label) in
            label?.textColor = .white
            label?.font = AppTheme.fontText(ofSize: 12.0)
        }
        // location labels
        [pickupLocationLabel, dropoffLabel].forEach { (label) in
            label?.textColor = .white
            label?.font = AppTheme.fontText(ofSize: 16.0)
        }
        // cash label
        cashLabel.textColor = .white
        cashLabel.font = AppTheme.fontText(ofSize: 14.0)
        // distance label
        distanceLabel.textColor = .white
        distanceLabel.font = AppTheme.fontText(ofSize: 14.0)
        // start button
        startButton.backgroundColor = AppTheme.colorSecondary
        startButton.setTitleColor(.white, for: .normal)
        startButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
        startButton.layer.cornerRadius = 10.0
        // cancel button
        cancelButton.setTitleColor(AppTheme.colorRed, for: .normal)
        cancelButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 16.0)
    }
    
    private func configureStatusView(){
        statusLabel.font = AppTheme.fontHeading(ofSize: 22.0)
        statusLabel.textColor = AppTheme.colorSecondary
        statusLabel.backgroundColor = UIColor(white: 1, alpha: 0.6)
        statusLabel.textAlignment = .center
        statusLabel.clipsToBounds = true
        statusLabel.layer.cornerRadius = 8
        showStatusView(with: CGRect(x: (self.view.bounds.width/2) - 125, y: 40, width: 250, height: 40))
        self.mapView.addSubview(statusLabel)
        mapView.bringSubviewToFront(statusLabel)
    }
    
    private func drawCurrentLocationMarker(){
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "driver-marker")
        marker.map = self.mapView
        marker.position = LocationManager.shared.lastReceivedLocation()!
    }
    
    private func drawDestinationMarker(){
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "drop-marker")
        marker.map = self.mapView
        marker.position = viewModel.destinationLocationOfDriver()
    }
    
    private func hideStatusView(){
        statusLabel.alpha = 0.0
    }
    
    private func showStatusView(with rect: CGRect){
        statusLabel.frame = rect
        statusLabel.alpha = 1.0
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? InRideViewController{
            destination.dataModel = viewModel.dataModel
        }
    }

    //MARK:- IBAction
    @IBAction func didTapCallButton(_ sender: Any) {
        UIApplication.shared.open(URL(string: "tel://\(viewModel.phoneNumberOfUser())")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func didTapMessageButton(_ sender: Any) {
    }
    
    @IBAction func didTapStartButton(_ sender: Any) {
        viewModel.didTapStartButton()
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        viewModel.cancelRide(success: {
            SessionManager.shared.removeCurrentTrip()
            self.showMessage(title: "Success", message: "Ride cancelled successfully", type: .success)
            self.navigationController?.popViewController(animated: true)
        }) {
            self.showMessage(title: "Error", message: "Error in cancelling ride", type: .error)
        }
    }
}

extension ArrivingToLocationViewController: ArriveToLocationDelegate{
    
    func didArrive() {
        startButton.setTitle("Start Ride", for: .normal)
        statusLabel.text = viewModel.textForStatusLabel()
    }
    
    func startRide() {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.showInRideFromArrivingLocation, sender: self.dataModel)
    }
    
    func updateValues(with model: NotificationDataModel) {
        pickupLocationLabel.text = model.ride?.source?.Name ?? "unknown"
        dropoffLabel.text = model.ride?.destination?.Name ?? "unknown"
        statusLabel.text = viewModel.textForStatusLabel()
        nameLabel.text = viewModel.nameOfUser()
        ratingLabel.text = viewModel.ratingOfUser()
        distanceLabel.text = viewModel.estimatedDistanceOfRide()
    }
    
    func customerCancelledRide() {
        self.showMessage(title: "Ride Cancelled", message: "Customer has cancelled the ride", type: .error)
        self.navigationController?.popViewController(animated: true)
    }
    
    func didGetUpdatedLocation() {
        mapView.drawPath(fromLocation: LocationManager.shared.lastReceivedLocation()!, toLocation: viewModel.destinationLocationOfDriver()) {
            self.drawCurrentLocationMarker()
            self.drawDestinationMarker()
        }
    }
}
