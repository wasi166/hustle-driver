
//
//  SideMenuViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 14/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    private let viewModel = SideMenuViewModel()
    override func viewDidLoad() {
        self.baseModel = viewModel
        viewModel.baseDelegate = self
        super.viewDidLoad()
    }
    
    override func configureUI() {
        super.configureUI()
        configureSideMenu()
        self.navigationController?.navigationBar.isHidden = true
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    
    private func configureSideMenu(){
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuEnableSwipeGestures = true
        SideMenuManager.default.menuShadowColor = .clear
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width * 0.8
    }
    
    //MARK:- TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = viewModel.dataForIndexPath(indexPath: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: data[BaseCellConstant.Identifier] as! String) as! BaseTableViewCell
        cell.configure(data: data, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: viewModel.identifierForSectionHeader()) as! SideMenuHeaderTableViewCell
        header.baseDelegate = self
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(viewModel.heightForSectionHeader())
    }
}

//MARK:- View model delegate
extension SideMenuViewController: SideMenuDelegate, SideMenuHeaderDelegate{
    
    func presentEditProfile() {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentEditProfileFromMap, sender: self)
    }
    
    func presentWallet() {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentWalletFromMap, sender: self)
    }
    
    func logout() {
        viewModel.sessionExpired()
    }
    
    func presentRideHistory() {
        self.performSegue(withIdentifier: UIConstants.Segues.Ride.presentRideHistory, sender: self)
    }
}
