//
//  StartViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 29/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import DropDown

class StartViewController: BaseViewController, StartViewModelDelegate {

    @IBOutlet weak var SingupLabel: UILabel!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet var fieldLeftView: UIView!
    @IBOutlet weak var leftFlagImageView: UIImageView!
    @IBOutlet weak var leftCountryCodeLabel: UILabel!
    
    private let viewModel = StartViewModel()
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        viewModel.delegate = self
    }
    
    override func configureUI() {
        super.configureUI()
        self.view.backgroundColor = AppTheme.colorScreenBackground
        // navigation controller
        self.navigationController?.navigationBar.isHidden = true
        // signup label
        self.SingupLabel.textColor = .black
        self.SingupLabel.font = AppTheme.fontHeading(ofSize: 24.0)
        //phone field
        self.phoneField.backgroundColor = .white
        self.phoneField.layer.cornerRadius = 10.0
        self.phoneField.font = AppTheme.fontText(ofSize: 14.0)
        self.phoneField.makeThemedTextField()
        self.phoneField.configureLeftView(view: self.fieldLeftView)
        // next Button
        self.nextButton.backgroundColor = AppTheme.colorPrimary
        self.nextButton.setTitleColor(.white, for: .normal)
        self.nextButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 18.0)
        self.nextButton.layer.cornerRadius = 10.0
        // field left view
        self.fieldLeftView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapFlagView)))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.Segues.Registration.startVCToSignin{
            if let phone = sender as? String{
                let signinViewController = segue.destination as! SigninViewController
                signinViewController.phoneNumber = phone
            }
        }
    }
    
    //MARK:- view model delegate
    func didSendTokenSuccessfully() {
        self.hideLoader()
        self.performSegue(withIdentifier: UIConstants.Segues.Registration.startVCToSignin, sender: viewModel.phoneNumber(with: phoneField.text!))
    }
    
    func failedToSendToken(withError: String) {
        self.hideLoader()
        self.showMessage(title: "Error", message: withError, type: .error)
    }
    
    //MARK:- IBActions
    @IBAction func didTapNextButton(_ sender: Any) {
        if self.viewModel.isValidPhoneNumber(number: viewModel.phoneNumber(with: self.phoneField.text!)){
            self.showLoader()
            viewModel.sendOTP(viewModel.phoneNumber(with: self.phoneField.text!))
        }else{
            showMessage(title: "Error", message: viewModel.errorForPhoneNumber(number: self.phoneField.text!), type: .error)
        }
    }
    
    @objc private func didTapFlagView(){
        let dropDown = DropDown()
        dropDown.anchorView = self.fieldLeftView
        dropDown.direction = .any
        dropDown.dataSource = viewModel.dataSourceForDropDown()
        dropDown.cellNib = UINib(nibName: CountryCodeDropDownCell.nibName, bundle: Bundle.main)
        dropDown.textFont = AppTheme.fontText(ofSize: 12.0)
        dropDown.dismissMode = .onTap
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? CountryCodeDropDownCell else { return }
            
            cell.logoImageView.image = self.viewModel.imageForCountry(at: index)
        }
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.setCountryCode(at: index)
            self.leftFlagImageView.image = self.viewModel.imageForCountry(at: index)
            self.leftCountryCodeLabel.text = self.viewModel.countryCode(at: index)
        }
        dropDown.show()
    }
}
