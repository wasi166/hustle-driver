//
//  SigninViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 01/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class SigninViewController: BaseViewController, SigninDelegate {

    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var dontHaveAccountLabel: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var counterButton: UIButton!
    
    var smsCounter = 30
    var phoneNumber = ""
    private var viewModel = SigninViewModel()
    
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        viewModel.delegate = self
        startTimer()
    }
    
    override func configureUI() {
        super.configureUI()
        
        self.view.backgroundColor = AppTheme.colorScreenBackground
        // phone field
        self.phoneField.backgroundColor = .white
        self.phoneField.layer.cornerRadius = 10.0
        self.phoneField.font = AppTheme.fontText(ofSize: 14.0)
        self.phoneField.makeThemedTextField()
        self.phoneField.configureLeftView()
        self.phoneField.text = self.phoneNumber
        // password field
        self.passwordField.backgroundColor = .white
        self.passwordField.layer.cornerRadius = 10.0
        self.passwordField.font = AppTheme.fontText(ofSize: 14.0)
        self.passwordField.makeThemedTextField()
        self.passwordField.configureLeftView()
        // sign in button
        self.signinButton.backgroundColor = AppTheme.colorSecondary
        self.signinButton.setTitleColor(.white, for: .normal)
        self.signinButton.titleLabel?.font = AppTheme.fontHeading(ofSize: 18.0)
        self.signinButton.layer.cornerRadius = 10.0
        // counter button
        self.counterButton.titleLabel?.font = AppTheme.fontText(ofSize: 14.0)
        self.counterButton.setTitleColor(AppTheme.colorText, for: .normal)
        self.counterButton.isUserInteractionEnabled = false
        
    }
    
    private func startTimer(){
        let _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            UIView.setAnimationsEnabled(false)
            self.smsCounter -= 1
            if self.smsCounter <= 0{
                timer.invalidate()
                self.counterButton.setTitle("Resend Pin", for: .normal)
                self.counterButton.layoutIfNeeded()
                self.smsCounter = 30
                self.counterButton.isUserInteractionEnabled = true
            }else{
                self.counterButton.setTitle("Time Remaining: 00:\(self.smsCounter)", for: .normal)
                self.counterButton.layoutIfNeeded()
                self.counterButton.isUserInteractionEnabled = false
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func didTapSigninButton(_ sender: Any) {
        
        if viewModel.isFormValid(number: self.phoneField.text!, token: self.passwordField.text!){
            self.showLoader()
            viewModel.validateToken(number: self.phoneField.text!, token: self.passwordField.text!)
        }else{
            self.showMessage(title: "Error", message: viewModel.errorForInvalidForm(), type: .error)
        }
    }
    
    @IBAction func didTapTimerButton(_ sender: Any) {
        self.showLoader()
        viewModel.sendOTP(self.phoneField.text!, success: {
            self.hideLoader()
            self.startTimer()
            self.showMessage(title: "Success", message: "Pin Code resent successfully", type: .success)
        }) { (error) in
            self.hideLoader()
            self.showMessage(title: "Error", message: error, type: .error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.Segues.Registration.signinVCtoSignupVC{
            if let destination = segue.destination as? SignupViewController{
                destination.token = (sender as? String) ?? ""
            }
        }
    }
    
    //MARK:- Signin delegate
    func didVerifyTokenSuccessfully(shouldLogin: Bool) {
        self.hideLoader()
        if shouldLogin{
            let mapViewController = UIStoryboard(name: UIConstants.Storyboards.ride, bundle: Bundle.main).instantiateViewController(withIdentifier: UIConstants.ViewControllerIdentifiers.Ride.mapNavigationVC)
            UIApplication.shared.delegate?.window??.rootViewController = mapViewController
        }else{
            self.showMessage(title: "Error", message: "No driver account for this phone number", type: .error)
        }
    }
    
    func didFailToVerifyToken(error: String) {
        self.hideLoader()
        showMessage(title: "Error", message: error, type: .error)
    }
}
