//
//  RideHistoryViewController.swift
//  Hustle
//
//  Created by  Wasi Tariq on 10/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class RideHistoryViewController: BaseViewController {

    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneBarItem: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    private let viewModel = RideHistoryViewModel()
    override func viewDidLoad() {
        self.baseModel = viewModel
        super.viewDidLoad()
        self.requestData()
    }
    
    override func configureUI() {
        super.configureUI()
        
        navigationView.backgroundColor = AppTheme.colorPrimary
        titleLabel.textColor = .white
        titleLabel.font = AppTheme.fontHeading(ofSize: 16.0)
        doneBarItem.setTitleColor(.white, for: .normal)
        doneBarItem.titleLabel?.font = AppTheme.fontText(ofSize: 16.0)
    }
    
    private func requestData(){
        self.showLoader()
        viewModel.callRideHistoryRequest({
            self.hideLoader()
            self.tableView.reloadData()
        }) { (error) in
            self.hideLoader()
            self.showMessage(title: NSLocalizedString("Error", comment: ""), message: error, type: .error)
        }
    }
    
    @IBAction func didTapDoneItemButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- UITableView Delegate
extension RideHistoryViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = viewModel.dataForIndexPath(indexPath: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: data[BaseCellConstant.Identifier] as! String) as! BaseTableViewCell
        cell.configure(data: data, indexPath: indexPath)
        return cell
    }
}
