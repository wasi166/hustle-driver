//
//  BaseDataModel.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 25/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

struct BaseDataModel: Decodable{
    var status: String?
}
