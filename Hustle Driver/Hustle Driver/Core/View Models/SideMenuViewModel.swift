//
//  SideMenuViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 14/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

struct SideMenuDataModel: Decodable{
    var title: String!
    var image: String!
}

protocol SideMenuDelegate {
    func logout()
    func presentWallet()
    func presentRideHistory()
}

class SideMenuViewModel: BaseViewModel {

    var dataModels = [SideMenuDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataModels = FileRead.getDataModel(for: .sideMenuDemo, ofType: ConfigurationsConstants.fileType)!
    }
    
    func numberOfSections() -> Int{
        return 1
    }
    
    func numberOfRowsInSection() -> Int{
        return dataModels.count
    }
    
    func dataForIndexPath(indexPath: IndexPath) -> Dictionary<String, Any>{
        var data = Dictionary<String, Any>()
        data[BaseCellConstant.Identifier] = SideMenuTableViewCell.identifier
        data["title"] = NSLocalizedString(dataModels[indexPath.row].title, comment: "")
        data["image"] = dataModels[indexPath.row].image
        return data
    }
    
    func didSelectRow(at indexPath: IndexPath){
        switch indexPath.row {
        case 0:
            (baseDelegate as? SideMenuDelegate)?.presentRideHistory()
        case 1:
            (baseDelegate as? SideMenuDelegate)?.logout()
        default:
            break
        }
    }
    
    func identifierForSectionHeader() -> String{
        return SideMenuHeaderTableViewCell.identifier
    }
    
    func heightForSectionHeader() -> Int {
        return 140
    }
}
