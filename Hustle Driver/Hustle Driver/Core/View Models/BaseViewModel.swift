//
//  BaseViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 29/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation
import UIKit

class BaseViewModel {

    weak var baseDelegate: AnyObject?
    
    func viewDidLoad(){
        
    }
    
    func viewWillAppear() {}
    
    func viewDidAppear() {}
    
    func viewWillDissappear() {}
    
    func viewDidDissappear() {}

    func getAPIHeaders() -> Dictionary<String, Any>{
        return [
            "token": SessionManager.shared.getToken(),
            "phone": SessionManager.shared.getPhoneNumber(),
            "UserType": "2"
        ]
    }
    
    func sessionExpired(){
        SessionManager.shared.logOut()
        UIApplication.shared.delegate?.window??.rootViewController = UIStoryboard(name: UIConstants.Storyboards.registration, bundle: Bundle.main).instantiateViewController(withIdentifier: UIConstants.ViewControllerIdentifiers.Registrations.startNavigationController)
    }
}
