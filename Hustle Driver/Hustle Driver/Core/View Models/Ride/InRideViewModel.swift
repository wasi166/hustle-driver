//
//  InRideViewModel.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 11/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps


protocol InRideDelegate {
    func updateValues()
    func didGetUpdatedLocation()
    func endTrip()
}

class InRideViewModel: BaseViewModel {
    var dataModel: NotificationDataModel!
    private var detailsScheduler: Scheduler?
    private var wayPoints = [CLLocationCoordinate2D]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsScheduler = Scheduler(url: ServiceConstants.rideDetailsEndPoint, timeInterval: 5.0, params: ["ride" : ["Id": dataModel!.ride?.Id ?? ""]])
        configureRideDetailsScheduler()
        addNotificationObservers()
        (baseDelegate as? InRideDelegate)?.didGetUpdatedLocation()
        (baseDelegate as? InRideDelegate)?.updateValues()
        self.wayPoints = SessionManager.shared.getWayPoints()
        self.wayPoints.append(LocationManager.shared.lastReceivedLocation()!)
      //   SessionManager.shared.saveWayPoints(array: wayPoints)
    }
    
    override func viewWillDissappear() {
        super.viewWillDissappear()
        detailsScheduler!.stopScheduler()
    }
    
    private func configureRideDetailsScheduler(){
        detailsScheduler!.startScheduler { (model: NotificationDataModel?) in
            if let model = model{
                self.dataModel = model
                (self.baseDelegate as? InRideDelegate)?.updateValues()
            }
        }
    }
    
    private func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(didGetUpdatedLocation), name: .receivedUpdatedLocation, object: nil)
    }
    
    @objc private func didGetUpdatedLocation(){
        let location = LocationManager.shared.lastReceivedLocation()!
        if let lastCoordinate = self.wayPoints.last{
            if (CLLocation(latitude: lastCoordinate.latitude, longitude: lastCoordinate.longitude).distance(from: CLLocation(latitude: location.latitude, longitude: location.longitude)) >= 4000){
                self.wayPoints.append(lastCoordinate)
            //    SessionManager.shared.saveWayPoints(array: self.wayPoints)
            }
        }else{
            self.wayPoints.append(location)
        }
        (baseDelegate as? InRideDelegate)?.didGetUpdatedLocation()
    }
    
    func didTapEndRideButton(){
        wayPoints.append(LocationManager.shared.lastReceivedLocation()!)// append the location where ride ended
       // SessionManager.shared.saveWayPoints(array: wayPoints) // save the way points just in case app or internet get closed.
        self.endRide(success: {
            self.getRideDetails {
                (self.baseDelegate as? InRideDelegate)?.endTrip()
            }
        }) {
            print("Error in ending ride")
        }
    }
    
    func destinationLocation() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: dataModel.ride?.destination?.Latitude ?? 0, longitude: dataModel.ride?.destination?.Longitude ?? 0)
    }
    
    func dropOffLocationName() -> String{
        return dataModel.ride?.destination?.Name ?? "unknown"
    }
    
    func remainingDistance() -> String{
        return dataModel.DistADurBwDvrADest?.Distance?.Text ?? "calculating"
    }
    
    func remainingTime() -> String{
        return dataModel.DistADurBwDvrADest?.Duration?.Text ?? "calculating"
    }
    
    func navigationLinkForRide() -> URL?{
        if let latitude = dataModel.ride?.destination?.Latitude , let longitude = dataModel.ride?.destination?.Longitude{
            return URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!
        }
        return nil
    }
    
    //MARK:- SERVICE CALLING
    
    private func parametersForEndRide() -> Parameters{
        var params = Dictionary<String, Any>()
        params["rideDetails"] = ["ride": ["Id": dataModel.ride?.Id ?? ""]]
        var wayPointsArray = [Dictionary<String, Any>]()
        for coordinate in self.wayPoints{
            wayPointsArray.append(["Latitude": Double(coordinate.latitude), "Longitude": Double(coordinate.longitude)])
        }
        params["lstwaypoints"] = wayPointsArray
        return Parameters(dictionary: params)
    }
    
    private func parametersForRideDetails() -> Parameters{
        var params = Dictionary<String, Any>()
        params["ride"] = ["Id": SessionManager.shared.getCurrentTripId()]
        return Parameters(dictionary: params)
    }
    
    private func getRideDetails(success: @escaping() -> Void){
        let service = Service(url: ServiceConstants.rideDetailsEndPoint, parameters: parametersForRideDetails(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: NotificationDataModel?, error) in
            if let model = model{
                self.dataModel = model
                success()
            }
        }
    }
    
    private func endRide(success: @escaping() -> Void, failure: @escaping() -> Void){
        let service = Service(url: ServiceConstants.endRideEndpoint, parameters: parametersForEndRide(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: NotificationDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                self.dataModel = model!
                success()
            }else{
                failure()
            }
        }
    }
}
