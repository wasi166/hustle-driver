//
//  MapViewModel.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 25/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

enum ScreenState{
    case start, rideRequest,
    goingToCustomerLocation,
    reachedCustomerLocation, inRide
}

protocol MapViewDelegate{
    func presentNewRideRequest(with model: NotificationDataModel)
    func presentArrivingVC(with model: NotificationDataModel)
    func presentInRideVC(with model: NotificationDataModel)
    func presentRateUserVC(with model: NotificationDataModel)
}


class MapViewModel: BaseViewModel {
    
    private var isOnline = true
    private var updateLocationScheduler: Scheduler!
    
    override func viewDidLoad() {
        updateLocationScheduler = Scheduler(url: ServiceConstants.updateDriverLocation, timeInterval: 4.0, params: parametersForUpdateLocation())
        self.updateLocationIfOnline()
        self.addNotificationObservers()
        self.checkForCurrentRide()
    }
    
    private func checkForCurrentRide(){
        if SessionManager.shared.isCurrentTripInProgress(){
            self.getRideDetails()
        }
    }
    
    private func updateLocationIfOnline(){
        if self.isOnline{
            updateLocationScheduler.startScheduler(params: { () -> Dictionary<String, Any> in
                return self.parametersForUpdateLocation()
            }) { (_: BaseDataModel?) in
            }
        }else{
            updateLocationScheduler.stopScheduler()
        }
    }
    
    private func toggleOnlineStatus(){
        self.isOnline = !self.isOnline
        self.updateLocationIfOnline()
    }
    
    private func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(didGetNewRideRequest(notification:)), name: .newRideRequest, object: nil)
    }
    
    //MARK:- Notification Observers
    @objc private func didGetNewRideRequest(notification: Notification){
        print("Received a new ride request")
        print(notification.object as Any)
        if let dataModel = notification.object as? NotificationDataModel{
            (baseDelegate as? MapViewDelegate)?.presentNewRideRequest(with: dataModel)
        }
    }
    
    //MARK:- API Parameters
    private func parametersForUpdateLocation() -> Dictionary<String, Any>{
        var params = Dictionary<String, Any>()
        params["driver"] = ["Id": SessionManager.shared.getID(),
                            "driverLocation": ["Latitude": LocationManager.shared
                                .lastReceivedLocation()?.latitude ?? 0.0,
                                               "Longitude":  LocationManager.shared.lastReceivedLocation()?.longitude ?? 0.0]]
        return params
    }
    
    private func parametersForChangeStatus() -> Parameters{
        var params = Dictionary<String, Any>()
        params["driver"] = ["Id": SessionManager.shared.getID(),
                            "status": ["Number": self.isOnline ? "2" : "1"]]
        return Parameters(dictionary: params)
    }
    
    private func parametersForAcceptRide(with rideID: String) -> Parameters{
        var params = Dictionary<String, Any>()
        params["ride"] = ["Id": rideID, "driver": ["Id": SessionManager.shared.getID()]]
        return Parameters(dictionary: params)
    }
    
    private func parametersForRideDetails() -> Parameters{
        var params = Dictionary<String, Any>()
        params["ride"] = ["Id": SessionManager.shared.getCurrentTripId()]
        return Parameters(dictionary: params)
    }
    
    // MARK:- Service Calling
    private func getRideDetails(){
        let service = Service(url: ServiceConstants.rideDetailsEndPoint, parameters: parametersForRideDetails(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: NotificationDataModel?, error) in
            if let model = model, let status = RideStatusTypes(rawValue: model.ride?.rideStatus?.Title ?? ""){
                switch status{
                case .accepted, .arrived:
                    (self.baseDelegate as? MapViewDelegate)?.presentArrivingVC(with: model)
                case .inRide:
                    (self.baseDelegate as? MapViewDelegate)?.presentInRideVC(with: model)
                case .finished:
                    (self.baseDelegate as? MapViewDelegate)?.presentRateUserVC(with: model)
                default:
                    break
                }
            }
        }
    }
    
    func changeOnlineStatus(success: @escaping () -> Void, _ failure: @escaping() -> Void){
        let service = Service(url: ServiceConstants.changeDriverStatusEndpoint, parameters: self.parametersForChangeStatus(), headers: self.getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                self.toggleOnlineStatus()
                success()
            }else {
                failure()
            }
        }
    }
    
    func acceptRide(with rideID: String, success: @escaping() -> Void, failure: @escaping() -> ()){
        let service = Service(url: ServiceConstants.acceptRideEndPoint, parameters: parametersForAcceptRide(with: rideID), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                success()
            }else {
                failure()
            }
        }
    }
}
