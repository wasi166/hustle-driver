//
//  ArrivingToLocationViewModel.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 07/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import GoogleMaps

protocol ArriveToLocationDelegate {
    func didArrive()
    func startRide()
    func customerCancelledRide()
    func didGetUpdatedLocation()
    func updateValues(with model: NotificationDataModel)
}

class ArrivingToLocationViewModel: BaseViewModel {
    
    var dataModel: NotificationDataModel!
    private var isArrived = false
    
    private var detailsScheduler: Scheduler?
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsScheduler = Scheduler(url: ServiceConstants.rideDetailsEndPoint, timeInterval: 5.0, params: ["ride" : ["Id": dataModel!.ride?.Id ?? ""]])
        (self.baseDelegate as? ArriveToLocationDelegate)?.updateValues(with: self.dataModel)
        (self.baseDelegate as? ArriveToLocationDelegate)?.didGetUpdatedLocation()
        self.addNotificationObservers()
        self.configureRideDetailsScheduler()
    }
    
    override func viewWillDissappear() {
        super.viewWillDissappear()
        detailsScheduler!.stopScheduler()
    }
    
    private func configureRideDetailsScheduler(){
        detailsScheduler!.startScheduler { (model: NotificationDataModel?) in
            if let model = model{
                self.dataModel = model
                (self.baseDelegate as? ArriveToLocationDelegate)?.updateValues(with: model)
            }
        }
    }
    
    private func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(customerCancelled), name: .customerCancelled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didGetUpdatedLocation), name: .receivedUpdatedLocation, object: nil)
    }
    
    @objc private func customerCancelled(){
        SessionManager.shared.removeCurrentTrip()
        (baseDelegate as? ArriveToLocationDelegate)?.customerCancelledRide()
    }
    
    @objc private func didGetUpdatedLocation(){
        (baseDelegate as? ArriveToLocationDelegate)?.didGetUpdatedLocation()
    }
    
    func didTapStartButton(){
        if isArrived{
            self.startRide(success: {
                (self.baseDelegate as? ArriveToLocationDelegate)?.startRide()
            }) {}
        }else{
            self.markArrived(success: {
                self.isArrived = true
                (self.baseDelegate as? ArriveToLocationDelegate)?.didArrive()
            }) {}
        }
    }
    
    func nameOfUser() -> String{
        return dataModel.ride?.customer?.getFullName() ?? ""
    }
    
    func ratingOfUser() -> String{
        return dataModel.ride?.customer?.getRatingText() ?? ""
    }
    
    func phoneNumberOfUser() -> String{
        return dataModel.ride?.customer?.PhoneNumber ?? ""
    }
    
    func textForStatusLabel() -> String{
        return isArrived ? "Arrived" : self.dataModel.DistADurBwCustADvr?.Duration?.Text ?? ""
    }
    
    func estimatedDistanceOfRide() -> String{
        return dataModel.ride?.estimatedFare?.getEstimatedDistanceInKms() ?? ""
    }
    
    func destinationLocationOfDriver() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: self.dataModel.ride?.source?.Latitude ?? 0, longitude: self.dataModel.ride?.source?.Longitude ?? 0)
    }
    
    func parametersForCancelRide() -> Parameters{
        var params = Dictionary<String, Any>()
        params["ride"] = ["Id": dataModel.ride?.Id ?? ""]
        return Parameters(dictionary: params)
    }
    
    
    //MARK:- Service Calling
    private func startRide(success: @escaping() -> (), failure: @escaping() -> ()){
        let service = Service(url: ServiceConstants.startRideEndpoint, parameters: parametersForCancelRide(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: NotificationDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                self.dataModel = model!
                (self.baseDelegate as? ArriveToLocationDelegate)?.updateValues(with: self.dataModel)
                success()
            }else {
                failure()
            }
        }
    }
    
    private func markArrived(success: @escaping() -> (), failure: @escaping() -> ()){
        let service = Service(url: ServiceConstants.driverArrivedEndpoint, parameters: parametersForCancelRide(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                success()
            }else {
                failure()
            }
        }
    }
    
    func cancelRide(success: @escaping() -> (), failure: @escaping() -> ()){
        let service = Service(url: ServiceConstants.cancelRideByDriver, parameters: parametersForCancelRide(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                success()
            }else {
                failure()
            }
        }
    }
}
