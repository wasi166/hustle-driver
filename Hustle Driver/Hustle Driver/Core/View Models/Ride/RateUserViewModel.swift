//
//  RateUserViewModel.swift
//  Hustle Driver
//
//  Created by  Wasi Tariq on 14/09/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol RateUserDelegate {
    func presentSaveFareView(fare: Double)
    func didSaveFareSuccessfully()
    func failToSaveFare()
}

class RateUserViewModel: BaseViewModel {

    var dataModel: NotificationDataModel!
    private var isGotPaid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func totalFare() -> String {
        return "\(dataModel.ride?.rideFare?.TotalFare ?? 0.0) SAR"
    }
    
    func totalDistanceOfRide() -> String{
        return dataModel.ride?.rideFare?.TotalFareTime ?? ""
    }
    
    func totalDurationOfRide() -> String{
        return dataModel.ride?.rideFare?.totalDistanceInKms() ?? ""
    }
    
    func didTapPaidButton(){
        if !isGotPaid{
            (self.baseDelegate as? RateUserDelegate)?.presentSaveFareView(fare: self.dataModel.ride?.rideFare?.TotalFare ?? 0.0)
        }
    }
    
    func canDismiss() -> Bool{
        return self.isGotPaid
    }
    
    private func parametersForSaveRideFare(amount: Double) -> Parameters{
        var params = Dictionary<String, Any>()
        params["ride"] = ["Id": dataModel.ride?.Id ?? "",
                          "rideFare": ["TotalFare": dataModel.ride?.rideFare?.TotalFare ?? 0.0,
                                       "TotalFarePaid": amount]]
        return Parameters(dictionary: params)
    }
    
    private func parametersForSendRate(rating: Int) -> Parameters{
        var params = Dictionary<String, Any>()
        params["feedback"] =  ["ride": ["Id": dataModel.ride?.Id ?? ""],
                               "ComplimentText": "",
                               "TotalStars": 5]
        return Parameters(dictionary: params)
    }
    
    
    
    func sendRate(rating: Int, success: @escaping() -> Void, failure: @escaping() -> Void){
        let service = Service(url: ServiceConstants.driverFeebackEndpoint, parameters: parametersForSendRate(rating: rating), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                success()
            }else{
                failure()
            }
        }
    }
    
    func saveRideFare(amount: Double){
        let service = Service(url: ServiceConstants.saveRideFare, parameters: parametersForSaveRideFare(amount: amount), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: BaseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("success"){
                self.isGotPaid = true
                (self.baseDelegate as? RateUserDelegate)?.didSaveFareSuccessfully()
            }else{
                (self.baseDelegate as? RateUserDelegate)?.failToSaveFare()
            }
        }
    }
}
