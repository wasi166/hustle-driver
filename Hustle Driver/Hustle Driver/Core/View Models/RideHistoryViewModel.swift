//
//  RideHistoryViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 10/08/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

class RideHistoryViewModel: BaseViewModel {

    private var rideHistory = [RideHistoryDataModel]()
    
    private func parametersForRideHistory() -> Parameters{
        var paramsDictionary = Dictionary<String, Any>()
        paramsDictionary["driverDetails"] = ["Id": SessionManager.shared.getID()]
        return Parameters(dictionary: paramsDictionary)
    }
    
    func numberOfRows() -> Int{
        return rideHistory.count
    }
    
    func dataForIndexPath(indexPath: IndexPath) -> Dictionary<String, Any>{
        var data = Dictionary<String, Any>()
        data[BaseCellConstant.Identifier] = RideHistoryTableViewCell.identifier
        data["data"] = rideHistory[indexPath.row]
        return data
    }
    
    func callRideHistoryRequest(_ success: @escaping() -> Void, failure: @escaping(String) -> Void)
    {
        let service = Service(url: ServiceConstants.rideHistoryEndpoint, parameters: parametersForRideHistory(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: RideHistoryResponseDataModel?, error) in
            if (model?.status ?? "").lowercased().contains("succes"){
                self.rideHistory = model?.driverDetails?.lstRideDetails ?? []
                success()
            }else{
                failure(NSLocalizedString("There was some error in getting ride history. Try again later", comment: ""))
            }
        }
    }
    
}
