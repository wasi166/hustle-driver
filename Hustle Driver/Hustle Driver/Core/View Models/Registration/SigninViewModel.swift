//
//  SigninViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 01/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit

protocol SigninDelegate: class {
    func didVerifyTokenSuccessfully(shouldLogin: Bool)
    func didFailToVerifyToken(error: String)
}

class SigninViewModel: BaseViewModel {

    weak var delegate: SigninDelegate?
    
    func isFormValid(number: String, token: String) -> Bool{
        return !(number.isEmpty || token.isEmpty)
    }
    
    func errorForInvalidForm() -> String{
        return "Values cannot be empty"
    }
    
    private func urlForPendingRides() -> String{
        return "\(ServiceConstants.pendingRidesEndpoint)?DriverId=\(SessionManager.shared.getID())"
    }
    
    private func callPendingRidesRequest(_ success: @escaping() -> Void){
        let service = Service(url: urlForPendingRides(), parameters: Parameters(), headers: getAPIHeaders())
        ServiceManager.shared.request(service: service) { (model: NotificationDataModel?, error) in
            if let model = model{
                if let rideID = model.ride?.Id{
                    SessionManager.shared.setCurrentTrip(id: rideID)
                }
                success()
            }
        }
    }
    
    func sendOTP(_ number: String, success: @escaping() -> Void, failure: @escaping(String) -> Void){
        let url = ServiceConstants.validateNumberEndpoint+"/"+number
        ServiceManager.shared.getRequest(url: url) { (model: ValidateNumberDataModel?, error) in
            if let model = model{
                if (model.status ?? "").lowercased().contains("success"){
                    success()
                }else{
                    failure("Error in resending Authorization token.")
                }
            }else{
                failure("Error in resending Authorization token.")
            }
        }
    }
    
    func validateToken(number: String, token: String){
        let url = ServiceConstants.verifyNumberEndpoint+"/"+number+"/"+token+"/"+SessionManager.shared.getFCMToken()
        ServiceManager.shared.getRequest(url: url) { (model: ValidateNumberDataModel?, error) in
            if let model = model{
                if (model.status ?? "").lowercased().contains("success"){
                    let shouldLogin = model.driverData != nil
                    if shouldLogin{
                        SessionManager.shared.loginWith(AccountInfo: model.driverData!, token: (model.token) ?? "")
                        self.callPendingRidesRequest {
                            self.delegate?.didVerifyTokenSuccessfully(shouldLogin: shouldLogin)
                        }
                    }else{
                        self.delegate?.didVerifyTokenSuccessfully(shouldLogin: false)
                    }
                }else{
                    self.delegate?.didFailToVerifyToken(error: "Unable to verify the token")
                }
            }
        }
    }
}
// 4bcc6cce19444861bac2a9f32def003e07729a82
