//
//  StartViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 29/06/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import UIKit
import Alamofire

enum CountryCodeType:String, CaseIterable{
    case arab = "arab"
    case pak = "pak"
    
    static func dataSource() -> [String]{
        return CountryCodeType.allCases.map { (code) -> String in
            code.countryCode()
        }
    }
    
    func rawValue() -> String{
        switch self {
        case .arab:
            return "arab"
        case .pak:
            return "pak"
        }
    }
    
    func countryCodeString() -> String{
        return String(describing: self.countryCode().dropFirst())
    }
    
    func countryCode() -> String{
        switch self {
        case .pak:
            return "+92"
        case .arab:
            return "+966"
        }
    }
    
    func flagImage() -> UIImage{
        switch self {
        case .pak:
            return #imageLiteral(resourceName: "pakistan-flag")
        case .arab:
            return #imageLiteral(resourceName: "saudi-flag")
        }
    }
}

protocol StartViewModelDelegate: class {
    func didSendTokenSuccessfully()
    func failedToSendToken(withError: String)
}

class StartViewModel: BaseViewModel {
    
    private let numberDigitsLimit = 12
    weak var delegate: StartViewModelDelegate?
    private var countrySelected = CountryCodeType.arab
    
    func isValidPhoneNumber(number: String) -> Bool{
        return number.count == numberDigitsLimit
    }
    
    func dataSourceForDropDown() -> [String]{
        return CountryCodeType.dataSource()
    }
    
    func countryCode(at index: Int) -> String{
        return CountryCodeType.allCases[index].countryCode()
    }
    
    func imageForCountry(at index: Int) -> UIImage{
        return CountryCodeType.allCases[index].flagImage()
    }
    
    func setCountryCode(at index: Int){
        self.countrySelected = CountryCodeType.allCases[index]
        SessionManager.shared.setCurrentCountryCode(self.countrySelected)
    }
    
    func phoneNumber(with userInputText: String) -> String{
        var phoneNumber = userInputText
        if !(phoneNumber.hasPrefix(self.countrySelected.countryCodeString())){
            if phoneNumber.hasPrefix("0"){
                phoneNumber = String(phoneNumber.dropFirst())
            }
            phoneNumber = self.countrySelected.countryCodeString() + phoneNumber
        }
        return phoneNumber
    }
    
    func errorForPhoneNumber(number: String) -> String{
        var error = ""
        if number.isEmpty{
            error = "Phone Number cannot be left empty"
        } else if number.count != numberDigitsLimit || !number.hasPrefix("966"){
            error = "Invalid format of Phone number"
        }
        return error
    }
    
    func sendOTP(_ number: String){
        let url = ServiceConstants.validateNumberEndpoint+"/"+number
        ServiceManager.shared.getRequest(url: url) { (model: ValidateNumberDataModel?, error) in
            if let model = model{
                if (model.status ?? "").lowercased().contains("success"){
                    self.delegate?.didSendTokenSuccessfully()
                }else{
                    self.delegate?.failedToSendToken(withError: "Unable to send uthorization token")
                }
            }else{
                self.delegate?.failedToSendToken(withError: "Unable to send uthorization token")
            }
        }
    }
}
