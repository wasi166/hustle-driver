//
//  ApplicationViewModel.swift
//  Hustle
//
//  Created by  Wasi Tariq on 25/07/2019.
//  Copyright © 2019 Wasi Tariq. All rights reserved.
//

import Foundation

class ApplicationViewModel: BaseViewModel{
    
    
    func textForPickupLocationLabel() -> String{
        return "this is a pickup location text"
    }
    
    func textForDropLocationLabel() -> String{
        return "this is a drop location text"
    }
    
    func nameOfUser() -> String{
        return "Wasi Tariq"
    }
    
    func ratingOfUser() -> String{
        return "5/5"
    }
    
    func phoneNumberOfUser() -> URL{
        return URL(string: "tel://")!
    }
    
    
}
